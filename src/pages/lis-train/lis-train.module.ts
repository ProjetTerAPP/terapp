import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LisTrainPage } from './lis-train';

@NgModule({
  declarations: [
    LisTrainPage,
  ],
  imports: [
    IonicPageModule.forChild(LisTrainPage),
  ],
})
export class LisTrainPageModule {}
